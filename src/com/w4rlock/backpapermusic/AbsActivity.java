package com.w4rlock.backpapermusic;

import android.annotation.SuppressLint;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.w4rlock.backpapermusic.loaders.LoadingInterface;
import com.w4rlock.backpapermusic.util.L;
import com.w4rlock.backpapermusic.util.Utils;

public class AbsActivity extends ActionBarActivity implements LoadingInterface {

	private View mSyncProgressBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		super.onCreate(savedInstanceState);
	}

	@SuppressLint("InlinedApi")
	private void ensureProgressBars() {
		if (Utils.isOverHoneyComb() && mSyncProgressBar == null) {
			int actionBarHeight;
			final TypedArray styledAttributes = getApplicationContext().getTheme().obtainStyledAttributes(
					new int[] { android.R.attr.actionBarSize });
			actionBarHeight = (int) styledAttributes.getDimension(0, 0);
			styledAttributes.recycle();

			final LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
			View v = inflater.inflate(R.layout.smooth_progress_bar, null, false);

			mSyncProgressBar = v.findViewById(R.id.progress);
			L.d("progressbar", "" + actionBarHeight);
			mSyncProgressBar.setMinimumHeight(actionBarHeight);
			if (getActionBarView() != null) {
				((ViewGroup) getActionBarView()).addView(v);
			} else {
				L.d("progressbar", "Null on getting action bar view");
			}
		}
		if (Utils.isOverHoneyComb()) {
			setSupportProgressBarIndeterminateVisibility(false);
		}
	}

	protected void showSyncStatusBar() {
		if (Utils.isOverHoneyComb()) {
			L.d("progressbar", "show sync status bar");
			ensureProgressBars();
			mSyncProgressBar.setVisibility(View.VISIBLE);
		} else {
			setSupportProgressBarIndeterminateVisibility(true);
		}
	}

	protected void hideSyncStatusBar() {
		if (Utils.isOverHoneyComb()) {
			L.d("progressbar", "show sync status bar");
			ensureProgressBars();
			mSyncProgressBar.setVisibility(View.GONE);
		} else {
			setSupportProgressBarIndeterminateVisibility(false);
		}
	}

	public View getActionBarView() {
		Window window = getWindow();
		View v = window.getDecorView();
		int resId = getResources().getIdentifier("action_bar_container", "id", "android");
		return v.findViewById(resId);
	}

	@Override
	public void onLoadingStarted() {
		showSyncStatusBar();
	}

	@Override
	public void onLoadingEnded() {
		hideSyncStatusBar();
	}

	@Override
	public void onProgressUpdate(int percent) {

	}
}
