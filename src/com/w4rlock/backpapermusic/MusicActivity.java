package com.w4rlock.backpapermusic;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.w4rlock.backpapermusic.database.service.MusicDatabaseSyncerService;
import com.w4rlock.backpapermusic.fragments.MusicListFragment;
import com.w4rlock.backpapermusic.loaders.LoadingInterface;
import com.w4rlock.backpapermusic.util.L;

public class MusicActivity extends AbsActivity {
	MusicListFragment musicListFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		L.d("MusicActivity", "oncreate");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_music);
		musicListFragment = new MusicListFragment();
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction().add(R.id.container, musicListFragment).commit();
		}
		com.nostra13.universalimageloader.utils.L.disableLogging();

		L.d("MusicActivity", "Calling Service");
		MusicDatabaseSyncerService musicDatabaseSyncerService = new MusicDatabaseSyncerService(getApplicationContext(),
				loadingInterface);
		musicDatabaseSyncerService.execute();
	}

	@Override
	public void onPause() {
		L.d("MusicActivity", "onPause");
		super.onPause();
	}

	@Override
	public void onResume() {
		L.d("MusicActivity", "onResume");
		super.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	final LoadingInterface loadingInterface = new LoadingInterface() {

		@Override
		public void onProgressUpdate(int percent) {

		}

		@Override
		public void onLoadingStarted() {
			showSyncStatusBar();
		}

		@Override
		public void onLoadingEnded() {
			hideSyncStatusBar();
			musicListFragment.forceLoadDataFromLoader();
		}
	};
}
