package com.w4rlock.backpapermusic.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.w4rlock.backpapermusic.R;
import com.w4rlock.backpapermusic.models.Song;
import com.w4rlock.backpapermusic.util.Utils;

public class MusicListViewAdapter extends ArrayAdapter<Song> {
	List<Song> music;
	ImageLoader imageLoader;
	DisplayImageOptions doptions = Utils.getDisplayImageOptions();
	private LayoutInflater inflater;
	static int resourceId = R.layout.each_album;

	public MusicListViewAdapter(Context context, List<Song> music) {
		super(context, resourceId, music);
		this.music = music;
		imageLoader = ImageLoader.getInstance();
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).build();
		imageLoader.init(config);
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		AlbumViewHolder albumViewHolder;
		if (row == null) {
			row = inflater.inflate(R.layout.each_album, parent, false);
			albumViewHolder = new AlbumViewHolder();
			albumViewHolder.songIcon = (ImageView) row.findViewById(R.id.album_icon);
			albumViewHolder.songLabel = (TextView) row.findViewById(R.id.album_label);
			row.setTag(albumViewHolder);
		} else {
			albumViewHolder = (AlbumViewHolder) row.getTag();
		}
		Song m = this.music.get(position);
		albumViewHolder.songLabel.setText(m.getDirname());
		imageLoader.displayImage(m.getThumbnail(), albumViewHolder.songIcon, doptions);
		return row;
	}

	static class AlbumViewHolder {
		public ImageView songIcon;
		public TextView songLabel;
	}
}
