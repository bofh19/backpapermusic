package com.w4rlock.backpapermusic.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.w4rlock.backpapermusic.R;
import com.w4rlock.backpapermusic.models.SongItem;
import com.w4rlock.backpapermusic.util.Utils;

public class PlayListDisplayAdapter extends ArrayAdapter<SongItem> {
	private LayoutInflater inflater;
	ImageLoader imageLoader;
	DisplayImageOptions doptions = Utils.getDisplayImageOptions();

	public PlayListDisplayAdapter(Context context, ArrayList<SongItem> songItems) {
		super(context, R.layout.each_song, songItems);
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		imageLoader = ImageLoader.getInstance();
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).build();
		imageLoader.init(config);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		SongViewHolder songViewHolder;
		if (row == null) {
			row = inflater.inflate(R.layout.playlist_each_song, parent, false);
			songViewHolder = new SongViewHolder();
			songViewHolder.songLabel = (TextView) row.findViewById(R.id.song_label);
			songViewHolder.songIcon = (ImageView) row.findViewById(R.id.song_icon);
			songViewHolder.songLableParent = (LinearLayout) row.findViewById(R.id.song_lable_parent_ll);
			row.setTag(songViewHolder);
		} else {
			songViewHolder = (SongViewHolder) row.getTag();
		}
		SongItem songItem = getItem(position);
		songViewHolder.songLabel.setText(songItem.getTitle());
		imageLoader.displayImage(songItem.getThumbUrl(), songViewHolder.songIcon, doptions);
		return row;
	}

	static class SongViewHolder {
		public LinearLayout songLableParent;
		public TextView songLabel;
		public ImageView songIcon;
	}
}
