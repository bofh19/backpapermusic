package com.w4rlock.backpapermusic.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.w4rlock.backpapermusic.R;
import com.w4rlock.backpapermusic.models.Music;
import com.w4rlock.backpapermusic.models.Song;
import com.w4rlock.backpapermusic.models.SongItem;

public class SongItemListDiaplayAdapter extends ArrayAdapter<SongItem> {
	private LayoutInflater inflater;

	public SongItemListDiaplayAdapter(Context context, ArrayList<SongItem> songs) {
		super(context, R.layout.each_song, songs);
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		SongViewHolder songViewHolder;
		if (row == null) {
			row = inflater.inflate(R.layout.each_song, parent, false);
			songViewHolder = new SongViewHolder();
			songViewHolder.songLabel = (TextView) row.findViewById(R.id.song_label);
			songViewHolder.songLableParent = (LinearLayout) row.findViewById(R.id.song_lable_parent_ll);
			row.setTag(songViewHolder);
		} else {
			songViewHolder = (SongViewHolder) row.getTag();
		}
		SongItem songItem = getItem(position);
		songViewHolder.songLabel.setText(songItem.getTitle());
		if (songItem.isSelected) {
			songViewHolder.songLableParent.setBackgroundColor(getContext().getResources().getColor(
					R.color.holo_blue_light));
		} else {
			songViewHolder.songLableParent.setBackgroundColor(getContext().getResources().getColor(R.color.white));
		}
		return row;
	}

	static class SongViewHolder {
		public LinearLayout songLableParent;
		public TextView songLabel;
	}
}
