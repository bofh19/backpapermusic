package com.w4rlock.backpapermusic.database.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class MusicDb {
	public static SQLiteDatabase getWriteDatabase(Context context) {
		MusicDatabaseSQLiteOpenHelper sqliteHelper = new MusicDatabaseSQLiteOpenHelper(context);
		return sqliteHelper.getWritableDatabase();
	}

	public static SQLiteDatabase getReadDatabase(Context context) {
		MusicDatabaseSQLiteOpenHelper sqliteHelper = new MusicDatabaseSQLiteOpenHelper(context);
		return sqliteHelper.getReadableDatabase();
	}
}
