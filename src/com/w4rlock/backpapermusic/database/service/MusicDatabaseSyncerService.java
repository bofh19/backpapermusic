package com.w4rlock.backpapermusic.database.service;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

import java.lang.reflect.Type;
import java.util.ArrayList;

import nl.qbusict.cupboard.QueryResultIterable;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.w4rlock.backpapermusic.database.db.MusicDb;
import com.w4rlock.backpapermusic.loaders.GetUrlRawData;
import com.w4rlock.backpapermusic.loaders.LoadingInterface;
import com.w4rlock.backpapermusic.models.DataSyncDate;
import com.w4rlock.backpapermusic.models.Song;
import com.w4rlock.backpapermusic.util.L;

// using this as async task for now to test
public class MusicDatabaseSyncerService extends AsyncTask<Void, Void, Void> {
	private String url = "http://backpapermusic.w4rlock.in/api/music_list/";
	private String modified_url = "http://backpapermusic.w4rlock.in/api/modified_date/";
	private Context context;
	private static final String DEBUG_TAG = MusicDatabaseSyncerService.class.toString();

	private LoadingInterface loadingInterface;

	public MusicDatabaseSyncerService(Context context, LoadingInterface loadingInterface) {
		this.context = context;
		this.loadingInterface = loadingInterface;
	}

	@Override
	protected void onPreExecute() {
		loadingInterface.onLoadingStarted();
		super.onPreExecute();
	}

	@Override
	protected void onPostExecute(Void result) {
		loadingInterface.onLoadingEnded();
		super.onPostExecute(result);
	}

	@Override
	protected Void doInBackground(Void... params) {
		L.d(DEBUG_TAG, "starting ");
		boolean goForUpdate = false;
		GetUrlRawData getUrlRawData = new GetUrlRawData();
		Gson gson = new Gson();
		DataSyncDate lastServerModifed = null;
		try {
			String lastModfiedResult = getUrlRawData.getData(modified_url);
			lastServerModifed = gson.fromJson(lastModfiedResult, DataSyncDate.class);
			L.d(DEBUG_TAG, "lastServerModifed.getGenerated_date: " + lastServerModifed.getGenerated_date());
			L.d(DEBUG_TAG, "DataSyncDate.getLastGeneratedDate: " + DataSyncDate.getLastGeneratedDate(context));
			if (lastServerModifed.getGenerated_date().equals(DataSyncDate.getLastGeneratedDate(context)))
				goForUpdate = false;
			else
				goForUpdate = true;
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		if (!goForUpdate) {
			L.d(DEBUG_TAG, "go for update says there is no need to update the db so quitting");
			return null;
		}
		try {
			String result = getUrlRawData.getData(url);

			ArrayList<ArrayList<Song>> music = new ArrayList<ArrayList<Song>>();

			Type listType = new TypeToken<ArrayList<ArrayList<Song>>>() {
			}.getType();
			music = gson.fromJson(result, listType);
			SQLiteDatabase db = MusicDb.getWriteDatabase(context);
			try {
				L.d(DEBUG_TAG, "clearing all tables");
				cupboard().withDatabase(db).dropAllTables();
				cupboard().withDatabase(db).createTables();
				L.d(DEBUG_TAG, "finished clearing all tables");
			} catch (Exception e) {
				L.d(DEBUG_TAG, "failed to drop and or create all tables");
				e.printStackTrace();
			}
			int i = 0;
			for (ArrayList<Song> songs : music) {
				i++;
				L.d(DEBUG_TAG, "writing set element " + i);
				cupboard().withDatabase(db).put(songs);
			}
			db.close();
			L.d(DEBUG_TAG, "finished writing all set element ");
		} catch (Exception e) {
			L.d(DEBUG_TAG, "Error while fetching data " + e.toString());
			e.printStackTrace();
		}
		if (lastServerModifed != null)
			DataSyncDate.saveToPreferences(context, lastServerModifed.getGenerated_date());

		L.d(DEBUG_TAG, "pritng dirnames");
		// Get the cursor for this query
		SQLiteDatabase db = MusicDb.getReadDatabase(context);
		Cursor songs = cupboard().withDatabase(db).query(Song.class).groupBy("dirname").getCursor();
		QueryResultIterable<Song> itr = cupboard().withCursor(songs).iterate(Song.class);
		L.d(DEBUG_TAG, "Calling results");
		try {
			for (Song song : itr) {
				L.d(DEBUG_TAG, song.getDirname() + "/" + song.getAlbum() + "/" + song.getName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			// close the cursor
			db.close();
			songs.close();
			itr.close();
		}
		L.d(DEBUG_TAG, "ended ");
		return null;
	}

}
