package com.w4rlock.backpapermusic.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.w4rlock.backpapermusic.R;
import com.w4rlock.backpapermusic.adapters.PlayListDisplayAdapter;
import com.w4rlock.backpapermusic.service.PlayList;
import com.w4rlock.backpapermusic.service.google.MusicService;

public class MusicPlayListFragment extends Fragment {
	public MusicPlayListFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_music_play_list, container, false);
		final PlayListDisplayAdapter playListDisplayAdapter = new PlayListDisplayAdapter(getActivity(),
				PlayList.playList);
		ListView listView = (ListView) rootView.findViewById(R.id.musicPlaylistListView);
		listView.setAdapter(playListDisplayAdapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				PlayList.moveNthPosToFirst(position);
				playListDisplayAdapter.notifyDataSetChanged();
				Intent i = new Intent(MusicService.ACTION_URL);
				Uri uri = Uri.parse("http://nothing.com/");
				i.setData(uri);
				getActivity().startService(i);
				playListDisplayAdapter.notifyDataSetChanged();
			}
		});
		return rootView;
	}

}
