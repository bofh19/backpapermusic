package com.w4rlock.backpapermusic.fragments;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.w4rlock.backpapermusic.R;
import com.w4rlock.backpapermusic.models.SongItem;
import com.w4rlock.backpapermusic.service.binder.MusicServiceBinder;
import com.w4rlock.backpapermusic.service.binder.MusicServiceCallbackInterface;
import com.w4rlock.backpapermusic.service.google.MusicService;
import com.w4rlock.backpapermusic.service.google.MusicService.State;
import com.w4rlock.backpapermusic.util.GetSongThumbDrawable;
import com.w4rlock.backpapermusic.util.L;
import com.w4rlock.backpapermusic.util.Utils;

public class MusicPlayerFragment extends Fragment implements MusicServiceCallbackInterface {
	private static final String DEBUG_TAG = MusicPlayerFragment.class.toString();

	@Override
	public void onStart() {
		super.onStart();
		attachToService();
	}

	private void attachToService() {
		// Bind to LocalService
		if (Utils.isMusicServiceRunning(getActivity()))
		// binding only if music
		// service is already
		// running
		{
			Intent intent = new Intent(getActivity(), MusicService.class);
			getActivity().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
		} else
		// we set mSongItem to null and call
		// write song details
		// to set default values
		{
			mSongItem = null;
			writeSongDetails(false);
		}
	}

	private void detachFromService() {
		// Unbind from the service
		if (mBound) {
			getActivity().unbindService(mConnection);
			mBound = false;
		}
		handler.removeCallbacks(onEverySecond);
		detachMusicPlayerCallbacks();
		mMusicService = null;
	}

	@Override
	public void onStop() {
		super.onStop();
		detachFromService();
	}

	MusicService mMusicService;
	boolean mBound = false;
	private SeekBar mSeekBarPlayer;
	private TextView mMediaTime;
	private MediaPlayer mPlayer;
	private SongItem mSongItem;
	private TextView mSongDetails;
	private ImageView mSongThumb;
	private TextView mMediaPercent;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_music_player, container, false);
		mSongDetails = (TextView) rootView.findViewById(R.id.songDetails);
		mSeekBarPlayer = (SeekBar) rootView.findViewById(R.id.playerSeekbar);
		mMediaTime = (TextView) rootView.findViewById(R.id.mediaTime);
		mSongThumb = (ImageView) rootView.findViewById(R.id.songThumb);
		mMediaPercent = (TextView) rootView.findViewById(R.id.mediaPercent);
		mSeekBarPlayer.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			int progress;
			boolean fromUser;

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				L.d(DEBUG_TAG, "onStopTrackingTouch");
				if (fromUser && mPlayer.isPlaying()) {
					mPlayer.seekTo(progress);
				}
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				L.d(DEBUG_TAG, "onStartTrackingTouch");
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				this.fromUser = fromUser;
				this.progress = progress;
			}
		});

		((Button) rootView.findViewById(R.id.pauseButton)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				getActivity().startService(new Intent(MusicService.ACTION_PAUSE));
			}
		});

		((Button) rootView.findViewById(R.id.nextButton)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				getActivity().startService(new Intent(MusicService.ACTION_SKIP));
			}
		});
		((Button) rootView.findViewById(R.id.previousButton)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				getActivity().startService(new Intent(MusicService.ACTION_REWIND));
			}
		});
		((Button) rootView.findViewById(R.id.playButton)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				detachFromService();
				getActivity().startService(new Intent(MusicService.ACTION_PLAY));
				attachToService();

			}
		});

		((Button) rootView.findViewById(R.id.stopButton)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				getActivity().startService(new Intent(MusicService.ACTION_STOP));
			}
		});

		writeSongDetails(false);
		return rootView;
	}

	private void writeSongDetails(SongItem songItem, boolean onPreperaing) {
		mSongItem = songItem;
		writeSongDetails(onPreperaing);
	}

	private void writeSongDetails(boolean onPreparing) {
		if (mSongItem != null) {
			mSongDetails.setText(mSongItem.getTitle() + " - " + mSongItem.getAlbum() + " - " + mSongItem.getArtist()
					+ (onPreparing ? " (Loading)" : " (Playing)"));
			GetThumbnailDrawable getThumbnailDrawable = new GetThumbnailDrawable(mSongItem);
			getThumbnailDrawable.execute();
		} else {
			mSeekBarPlayer.setMax(0);
			mSeekBarPlayer.setProgress(0);
			mSongDetails.setText(getActivity().getString(R.string.playing_nothing));
			mMediaTime.setText(getActivity().getString(R.string.time_duration_as_zeros));
			mSongThumb.setImageResource(R.drawable.dummy_album_art);
		}

	}

	protected void attachMusicPlayerCallbacks() {
		mMusicService.unregisterCallbackListener(MusicPlayerFragment.this);
		mMusicService.registerCallbackListener(MusicPlayerFragment.this);
	}

	protected void detachMusicPlayerCallbacks() {
		if (mMusicService != null)
			mMusicService.unregisterCallbackListener(MusicPlayerFragment.this);
		mPlayer = null;
	}

	private ServiceConnection mConnection = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			mBound = false;
			detachMusicPlayerCallbacks();
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			// We've bound to LocalService, cast the IBinder and get
			// LocalService instance
			MusicServiceBinder binder = (MusicServiceBinder) service;
			mMusicService = binder.getService();
			mBound = true;
			attachMusicPlayerCallbacks();
		}
	};

	@Override
	public void onSongChanged(SongItem songItem) {
		L.d(DEBUG_TAG, "onSongChanged");
	}

	@Override
	public void onStateChanged(State state) {
		L.d(DEBUG_TAG, "onStateChanged: " + state.toString());
	}

	@Override
	public void onPause(SongItem songItem) {
		L.d(DEBUG_TAG, "onPause");
	}

	@Override
	public void onPlay(SongItem songItem, MediaPlayer mPlayer) {
		L.d(DEBUG_TAG, "onPlay");
		mSongItem = songItem;
		this.mPlayer = mPlayer;
		writeSongDetails(false);
		handler.postDelayed(onEverySecond, 1000);
		mMediaPercent.setText("");
	}

	@Override
	public void onMediaPlayerStop() {
		L.d(DEBUG_TAG, "onMediaPlayerStop");
		mSongItem = null;
		handler.removeCallbacks(onEverySecond);
		writeSongDetails(false);
	}

	@Override
	public void onSongCompletion() {
		L.d(DEBUG_TAG, "onSongCompletion");
	}

	@Override
	public void onPrepared(MediaPlayer mPlayer) {
		L.d(DEBUG_TAG, "onPrepared");
	}

	@Override
	public void onError(MediaPlayer mp, int what, int extra) {
		L.d(DEBUG_TAG, "onError");
	}

	@Override
	public void onMediaPlayerReleased() {
		L.d(DEBUG_TAG, "onMediaPlayerReleased");
	}

	@Override
	public void onMediaPlayerCreated() {
		L.d(DEBUG_TAG, "onMediaPlayerCreated");
	}

	@Override
	public void onPreparing(SongItem songItem) {
		L.d(DEBUG_TAG, "onPreparing");
		writeSongDetails(songItem, true);
	}

	@Override
	public void onBufferUpdate(MediaPlayer mp, int percent) {
		L.d(DEBUG_TAG, "onBufferUpdate " + percent);
		mMediaPercent.setText(" - " + percent);
	}

	// updating seekbar and timers
	private Runnable onEverySecond = new Runnable() {
		@Override
		public void run() {
			handler.removeCallbacks(onEverySecond);
			try {
				if (mPlayer != null && mPlayer.isPlaying()) {
					duration = mPlayer.getDuration();
					mSeekBarPlayer.setMax(duration);
					mSeekBarPlayer.setProgress(mPlayer.getCurrentPosition());
					updateTime();
				} else {
					mSeekBarPlayer.setMax(0);
					mSeekBarPlayer.setProgress(0);
				}
			} catch (Exception e) {
				L.d(DEBUG_TAG, "" + e.toString());
			}
			handler.postDelayed(onEverySecond, 1000);
		}
	};

	private int current = 0;
	private int duration = 0;
	private final Handler handler = new Handler();

	private void updateTime() {
		current = mPlayer.getCurrentPosition();
		// L.d(DEBUG_TAG, "duration - " + duration + " current- " + current);
		int dSeconds = (int) (duration / 1000) % 60;
		int dMinutes = (int) ((duration / (1000 * 60)) % 60);
		int dHours = (int) ((duration / (1000 * 60 * 60)) % 24);

		int cSeconds = (int) (current / 1000) % 60;
		int cMinutes = (int) ((current / (1000 * 60)) % 60);
		int cHours = (int) ((current / (1000 * 60 * 60)) % 24);

		if (dHours == 0) {
			mMediaTime.setText(String.format("%02d:%02d / %02d:%02d", cMinutes, cSeconds, dMinutes, dSeconds));
		} else {
			mMediaTime.setText(String.format("%02d:%02d:%02d / %02d:%02d:%02d", cHours, cMinutes, cSeconds, dHours,
					dMinutes, dSeconds));
		}
	}

	// get thumbnail drawable from cache if available or online
	class GetThumbnailDrawable extends AsyncTask<String, Void, Bitmap> {
		SongItem songItem;

		public GetThumbnailDrawable(SongItem songItem) {
			this.songItem = songItem;
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			if (!isAdded()) {
				return;
			}
			L.d(DEBUG_TAG, "onPostExecute");
			if (result != null) {
				mSongThumb.setImageBitmap(result);
			} else {
				L.d(DEBUG_TAG, "onPostExecute Result is Null");
				mSongThumb.setImageResource(R.drawable.dummy_album_art);
			}
		}

		@Override
		protected Bitmap doInBackground(String... params) {
			L.d(DEBUG_TAG, "getting thumbnail ");
			try {
				if (mSongItem != null && mSongItem.getThumbUrl() != null) {
					return GetSongThumbDrawable.thumbFromUrl(getActivity().getApplicationContext(),
							mSongItem.getThumbUrl());
				} else {
					return null;
				}

			} catch (Exception e) {
				L.d(DEBUG_TAG, "error thumbnail " + e.toString());
				e.printStackTrace();
				return null;
			}
		}

	}

}
