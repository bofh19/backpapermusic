package com.w4rlock.backpapermusic.loaders;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

import java.util.ArrayList;

import nl.qbusict.cupboard.QueryResultIterable;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.w4rlock.backpapermusic.database.db.MusicDb;
import com.w4rlock.backpapermusic.models.Song;
import com.w4rlock.backpapermusic.util.L;

public class LoadSongsList extends AsyncTask<Song, Void, ArrayList<Song>> {
	public static interface LoadSongsListCallback {
		public void onLoadingFinished(ArrayList<Song> songs);
	}

	private LoadingInterface loadingInterface;
	private Context context;
	private static final String DEBUG_TAG = LoadSongsList.class.toString();
	LoadSongsListCallback callback;

	public LoadSongsList(Context context, LoadingInterface loadingInterface, LoadSongsListCallback callback) {
		this.loadingInterface = loadingInterface;
		this.context = context;
		this.callback = callback;
	}

	@Override
	protected void onPreExecute() {
		loadingInterface.onLoadingStarted();
		super.onPreExecute();
	}

	@Override
	protected void onPostExecute(ArrayList<Song> result) {
		loadingInterface.onLoadingEnded();
		callback.onLoadingFinished(result);
		super.onPostExecute(result);
	}

	@Override
	protected ArrayList<Song> doInBackground(Song... params) {
		ArrayList<Song> results = new ArrayList<Song>();
		Song givenSong = params[0];
		SQLiteDatabase db = MusicDb.getReadDatabase(context);
		Cursor songs = cupboard().withDatabase(db).query(Song.class)
				.withSelection("dirname = ?", givenSong.getDirname()).getCursor();
		QueryResultIterable<Song> itr = cupboard().withCursor(songs).iterate(Song.class);
		L.d(DEBUG_TAG, "Calling results");
		try {
			for (Song song : itr) {
				results.add(song);
				L.d(DEBUG_TAG, song.getDirname() + "/" + song.getAlbum() + "/" + song.getName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			// close the cursor
			db.close();
			songs.close();
			itr.close();
		}
		return results;
	}

}
