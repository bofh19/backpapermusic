package com.w4rlock.backpapermusic.loaders;

public interface LoadingInterface {
	public void onLoadingStarted();

	public void onLoadingEnded();

	public void onProgressUpdate(int percent);
}
