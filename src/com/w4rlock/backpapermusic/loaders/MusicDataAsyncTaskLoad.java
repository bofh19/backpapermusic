package com.w4rlock.backpapermusic.loaders;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

import java.util.ArrayList;

import nl.qbusict.cupboard.QueryResultIterable;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.w4rlock.backpapermusic.database.db.MusicDb;
import com.w4rlock.backpapermusic.models.AsyncResult;
import com.w4rlock.backpapermusic.models.Song;
import com.w4rlock.backpapermusic.util.L;

public class MusicDataAsyncTaskLoad extends AsyncTask<Void, Void, AsyncResult<ArrayList<Song>>> {
	public static final String DEBUG_TAG = MusicDataAsyncTaskLoad.class.toString();

	public static interface MusicDataAsyncTaskCallback {
		public void onAsyncLoadComplete(AsyncResult<ArrayList<Song>> result);
	}

	Context context;
	LoadingInterface loadingInterface;
	MusicDataAsyncTaskCallback callback;

	public MusicDataAsyncTaskLoad(Context context, LoadingInterface loadingInterface,
			MusicDataAsyncTaskCallback callback) {
		this.context = context;
		this.loadingInterface = loadingInterface;
		this.callback = callback;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		loadingInterface.onLoadingStarted();
	}

	@Override
	protected void onPostExecute(AsyncResult<ArrayList<Song>> result) {
		super.onPostExecute(result);
		loadingInterface.onLoadingEnded();
		callback.onAsyncLoadComplete(result);
	}

	@Override
	protected AsyncResult<ArrayList<Song>> doInBackground(Void... params) {
		AsyncResult<ArrayList<Song>> result = new AsyncResult<ArrayList<Song>>();
		ArrayList<Song> musics = new ArrayList<Song>();
		SQLiteDatabase db = MusicDb.getReadDatabase(context);
		Cursor songs = cupboard().withDatabase(db).query(Song.class).groupBy("dirname").getCursor();
		QueryResultIterable<Song> itr = cupboard().withCursor(songs).iterate(Song.class);
		L.d(DEBUG_TAG, "Calling results");
		try {
			for (Song song : itr) {
				musics.add(song);
				L.d(DEBUG_TAG, song.getDirname() + "/" + song.getAlbum() + "/" + song.getName());
			}
			result.setResult(musics);
			result.setError(false);
			return result;
		} catch (Exception e) {
			result.setError(true);
			result.setException(e);
			e.printStackTrace();
			return result;
		} finally {

			// close the cursor
			db.close();
			songs.close();
			itr.close();
		}
	}

}
