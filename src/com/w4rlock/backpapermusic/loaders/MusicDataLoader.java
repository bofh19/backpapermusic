package com.w4rlock.backpapermusic.loaders;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

import java.util.ArrayList;

import nl.qbusict.cupboard.QueryResultIterable;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.widget.Toast;

import com.w4rlock.backpapermusic.database.db.MusicDb;
import com.w4rlock.backpapermusic.models.AsyncResult;
import com.w4rlock.backpapermusic.models.Song;
import com.w4rlock.backpapermusic.util.L;

public class MusicDataLoader extends AsyncTaskLoader<AsyncResult<ArrayList<Song>>> {
	AsyncResult<ArrayList<Song>> result;
	private static final String DEBUG_TAG = MusicDataLoader.class.toString();

	public MusicDataLoader(Context context) {
		super(context);
	}

	@Override
	public AsyncResult<ArrayList<Song>> loadInBackground() {
		AsyncResult<ArrayList<Song>> result = new AsyncResult<ArrayList<Song>>();
		ArrayList<Song> musics = new ArrayList<Song>();
		SQLiteDatabase db = MusicDb.getReadDatabase(getContext());
		Cursor songs = cupboard().withDatabase(db).query(Song.class).groupBy("dirname").getCursor();
		QueryResultIterable<Song> itr = cupboard().withCursor(songs).iterate(Song.class);
		L.d(DEBUG_TAG, "Calling results");
		try {
			for (Song song : itr) {
				musics.add(song);
				L.d(DEBUG_TAG, song.getDirname() + "/" + song.getAlbum() + "/" + song.getName());
			}
			result.setResult(musics);
			result.setError(false);
			return result;
		} catch (Exception e) {
			result.setError(true);
			result.setException(e);
			e.printStackTrace();
			return result;
		} finally {

			// close the cursor
			db.close();
			songs.close();
			itr.close();
		}
	}

	@Override
	public void deliverResult(AsyncResult<ArrayList<Song>> mMusics) {
		if (isReset()) {
			result = null;
		}
		result = mMusics;
		if (isStarted()) {
			super.deliverResult(result);
		}
	}

	@Override
	protected void onStartLoading() {
		L.d("MusicDataLoader", "x" + result);
		if (result != null && !result.isError()) {
			L.d("MusicDataLoader", "Result available delivering it");
			deliverResult(result);
		} else {
			L.d("MusicDataLoader", "Result not available loading now");
			forceLoad();
		}
	}

	@Override
	protected void onStopLoading() {
		cancelLoad();
	}

	/*
	 * Handles a request to cancel a load.
	 */
	@Override
	public void onCanceled(AsyncResult<ArrayList<Song>> mMusics) {
		super.onCanceled(mMusics);
	}

	/**
	 * Handles a request to completely reset the Loader.
	 */
	@Override
	protected void onReset() {
		super.onReset();
		result = null;
		onStopLoading();
	}

}
