package com.w4rlock.backpapermusic.models;

import java.io.Serializable;

public class AsyncResult<T extends Serializable> implements Serializable {
	public T getResult() {
		return result;
	}

	public Exception getException() {
		return exception;
	}

	public boolean isError() {
		return error;
	}

	public void setResult(T result) {
		this.result = result;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	private T result;
	private Exception exception;
	private boolean error;
	private int responseCode;
}