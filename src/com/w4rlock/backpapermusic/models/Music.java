package com.w4rlock.backpapermusic.models;

import java.io.Serializable;
import java.util.ArrayList;

public class Music extends Song implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String BASE_URL = "http://db.w4rlock.in/media/music/";

	public ArrayList<SongItem> getSongs() {
		ArrayList<SongItem> songs = new ArrayList<SongItem>();
		// for (int i = 0; i < getFiles().getMp3().size(); i++)
		// {
		// songs.add(new SongItem(this, i));
		// }
		return songs;
	}

	public String getDirname() {
		return dirname;
	}

	public Files getFiles() {
		return files;
	}

	public void setDirname(String dirname) {
		this.dirname = dirname;
	}

	public void setFiles(Files files) {
		this.files = files;
	}

	public String getThumbnail() {
		if (files.getJpg() != null && files.getJpg().size() > 0) {
			return files.getJpg().get(0);
		} else {
			return "";
		}
	}

	public String getBaseUrl() {
		if (dirname != null) {
			return BASE_URL + dirname + "/";
		} else {
			return "";
		}
	}

	public String getSongPath(int songId) {
		return getBaseUrl() + this.files.getMp3().get(songId).getName();
	}

	public String getSongName(int childPosition) {
		return getFiles().getMp3().get(childPosition).getName();
	}

	public String getAlbumName() {
		String albumName = null;
		try {
			albumName = getFiles().getMp3().get(0).getAlbum();
		} catch (Exception e) {
		}
		if (albumName == null)
			albumName = getDirname();
		return albumName;
	}

	private String dirname;
	private Files files;

	public static class Files implements Serializable {
		private static final long serialVersionUID = 1L;

		public ArrayList<MP3> getMp3() {
			return mp3;
		}

		public ArrayList<String> getJpg() {
			return jpg;
		}

		public void setMp3(ArrayList<MP3> mp3) {
			this.mp3 = mp3;
		}

		public void setJpg(ArrayList<String> jpg) {
			this.jpg = jpg;
		}

		private ArrayList<MP3> mp3;
		private ArrayList<String> jpg;
	}

	public static class MP3 implements Serializable {
		private static final long serialVersionUID = 1L;

		public String getAlbum() {
			return album;
		}

		public String getPublisher() {
			return publisher;
		}

		public String getName() {
			return name;
		}

		public String getTitle() {
			return title;
		}

		public Integer getTime_secs() {
			return time_secs;
		}

		public String getArtist() {
			return artist;
		}

		public Integer getTrack_num() {
			return track_num;
		}

		public String getBit_rate() {
			return bit_rate;
		}

		public String getGenre() {
			return genre;
		}

		public Integer getSize_bytes() {
			return size_bytes;
		}

		public void setAlbum(String album) {
			this.album = album;
		}

		public void setPublisher(String publisher) {
			this.publisher = publisher;
		}

		public void setName(String name) {
			this.name = name;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public void setTime_secs(Integer time_secs) {
			this.time_secs = time_secs;
		}

		public void setArtist(String artist) {
			this.artist = artist;
		}

		public void setTrack_num(Integer track_num) {
			this.track_num = track_num;
		}

		public void setBit_rate(String bit_rate) {
			this.bit_rate = bit_rate;
		}

		public void setGenre(String genre) {
			this.genre = genre;
		}

		public void setSize_bytes(Integer size_bytes) {
			this.size_bytes = size_bytes;
		}

		private String album;
		private String publisher;
		private String name;
		private String title;
		private Integer time_secs;
		private String artist;
		private Integer track_num;
		private String bit_rate;
		private String genre;
		private Integer size_bytes;
	}
}
