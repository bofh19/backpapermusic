package com.w4rlock.backpapermusic.models;

import java.io.Serializable;

public class Song implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long _id;
	private String album;
	private String publisher;
	private String thumb_url;
	private Integer hash_key;
	private String name;
	private String artist;
	private Integer time_secs;
	private String track_url;
	private String title;
	private Integer track_num;
	private String bit_rate;
	private String size_bytes;
	private String dirname;

	public Long get_id() {
		return _id;
	}

	public String getAlbum() {
		if (this.album == null)
			return dirname;
		else
			return album;
	}

	public String getPublisher() {
		return publisher;
	}

	public String getThumb_url() {
		return thumb_url;
	}

	public Integer getHash_key() {
		return hash_key;
	}

	public String getName() {
		return name;
	}

	public String getArtist() {
		return artist;
	}

	public Integer getTime_secs() {
		return time_secs;
	}

	public String getTrack_url() {
		return track_url;
	}

	public String getTitle() {
		return title;
	}

	public Integer getTrack_num() {
		return track_num;
	}

	public String getBit_rate() {
		return bit_rate;
	}

	public String getSize_bytes() {
		return size_bytes;
	}

	public String getDirname() {
		return dirname;
	}

	public void set_id(Long _id) {
		this._id = _id;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public void setThumb_url(String thumb_url) {
		this.thumb_url = thumb_url;
	}

	public void setHash_key(Integer hash_key) {
		this.hash_key = hash_key;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public void setTime_secs(Integer time_secs) {
		this.time_secs = time_secs;
	}

	public void setTrack_url(String track_url) {
		this.track_url = track_url;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setTrack_num(Integer track_num) {
		this.track_num = track_num;
	}

	public void setBit_rate(String bit_rate) {
		this.bit_rate = bit_rate;
	}

	public void setSize_bytes(String size_bytes) {
		this.size_bytes = size_bytes;
	}

	public void setDirname(String dirname) {
		this.dirname = dirname;
	}

	public static final String BASE_URL = "http://backpapermusic.w4rlock.in";

	public String getBaseUrl() {
		return BASE_URL;
	}

	public String getThumbnail() {
		if (this.thumb_url != null)
			return BASE_URL + this.thumb_url;
		else
			return null;
	}

	public String getAlbumName() {
		if (this.album == null)
			return "";
		else
			return this.album;
	}

	public String getSongName(int something) {
		return this.getSongName();
	}

	public String getSongPath(int something) {
		return this.getSongPath();
	}

	public String getSongName() {
		if (this.title == null)
			return this.name;
		else
			return this.title;
	}

	public String getSongPath() {
		return BASE_URL + this.track_url;
	}
}
