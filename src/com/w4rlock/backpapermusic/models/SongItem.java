package com.w4rlock.backpapermusic.models;

import java.io.Serializable;

public class SongItem implements Serializable {
	private static final long serialVersionUID = 1L;
	private Song song;
	public boolean isSelected = false;

	public SongItem(Song song) {
		this.song = song;
	}

	public String getArtist() {
		return this.song.getArtist();
	}

	public String getTitle() {
		return song.getSongName(0);
	}

	public String getAlbum() {
		return song.getAlbum();
	}

	public long getDuration() {
		try {
			Integer durSec = song.getTime_secs();
			long total = ((long) durSec) * 1000;
			return total;
		} catch (Exception e) {
			return 0;
		}
	}

	public String getThumbUrl() {
		return song.getThumbnail();
	}

	public String getSongPath() {
		return song.getSongPath();
	}

}
