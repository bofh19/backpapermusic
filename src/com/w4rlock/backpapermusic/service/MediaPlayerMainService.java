package com.w4rlock.backpapermusic.service;

import java.io.IOException;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.widget.Toast;

import com.w4rlock.backpapermusic.MusicActivity;
import com.w4rlock.backpapermusic.R;
import com.w4rlock.backpapermusic.fragments.MusicListLoaderFragment;
import com.w4rlock.backpapermusic.util.GetSongThumbDrawable;
import com.w4rlock.backpapermusic.util.L;

public class MediaPlayerMainService extends Service implements MediaPlayer.OnPreparedListener {
	private static final String DEBUG_TAG = "MediaPlayerMainService";
	public static final String ACTION_PLAY = "com.example.action.PLAY";
	MediaPlayer mMediaPlayer = null;
	public static final int MSG_TYPE_1 = 1001;
	public static final String MSG_TYPE_1_KEY_0 = "message_type_1_key_0";
	public static final String MSG_TYPE_1_KEY_1 = "message_type_1_key_1";
	public static final String MSG_TYPE_1_KEY_2 = "message_type_1_key_2";
	public static final int MSG_REGISTER_CLIENT = 1;
	public static final int MSG_UNREGISTER_CLIENT = 2;

	@Override
	public void onPrepared(MediaPlayer arg0) {
		mMediaPlayer.start();
	}

	// service message methods

	// Target we publish for clients to send messages to IncomingHandler.
	final Messenger mMessenger = new Messenger(new IncomingHandler());

	// Keeps track of all current registered clients.
	ArrayList<Messenger> mClients = new ArrayList<Messenger>();
	private static boolean isRunning = false;

	@Override
	public IBinder onBind(Intent intent) {
		return mMessenger.getBinder();
	}

	class IncomingHandler extends Handler { // Handler of incoming messages from
											// clients.
		@Override
		public void handleMessage(Message msg) {
			L.d(DEBUG_TAG, "recieved some message " + msg.what);
			switch (msg.what) {
			case MSG_REGISTER_CLIENT:
				mClients.add(msg.replyTo);
				break;
			case MSG_UNREGISTER_CLIENT:
				mClients.remove(msg.replyTo);
				break;
			case MSG_TYPE_1:
				// startPlayingThisSong(
				// msg.getData().getString(MSG_TYPE_1_KEY_0));
				Bundle b = msg.getData();
				int groupPosition = b.getInt(MSG_TYPE_1_KEY_0);
				int childPosition = b.getInt(MSG_TYPE_1_KEY_1);
				startPlayingThisSong(groupPosition, childPosition);
				break;
			default:
				super.handleMessage(msg);
			}
		}
	}

	private void sendMessageToUI(String valuetosend) {
		for (int i = mClients.size() - 1; i >= 0; i--) {
			try {
				// Send data as an Integer
				// mClients.get(i).send(Message.obtain(null, MSG_TYPE_1));

				// Send data as a String
				Bundle b = new Bundle();
				b.putString(MSG_TYPE_1_KEY_0, valuetosend);
				Message msg = Message.obtain(null, MSG_TYPE_1);
				msg.setData(b);
				mClients.get(i).send(msg);

			} catch (RemoteException e) {
				// The client is dead. Remove it from the list; we are going
				// through the list from back to front so this is safe to do
				// inside the loop.
				mClients.remove(i);
			}
		}
	}

	public void startPlayingThisSong(int groupPosition, int childPosition) {
		String songUrl = MusicListLoaderFragment.music.get(groupPosition).getSongPath(childPosition);

		L.d(DEBUG_TAG, "Playing Song " + songUrl);
		if (mMediaPlayer == null) {
			Toast.makeText(getApplicationContext(), "Unable to Play Please Try Again", Toast.LENGTH_SHORT).show();
			return;
		}
		if (mMediaPlayer.isPlaying()) {
			mMediaPlayer.stop();
			mMediaPlayer.reset();
		}
		try {
			mMediaPlayer.setDataSource(songUrl);
			mMediaPlayer.setOnPreparedListener(this);
			mMediaPlayer.prepareAsync();
			Notification notification = getNotification("Playing: "
					+ MusicListLoaderFragment.music.get(groupPosition).getSongName(childPosition) + "\n"
					+ MusicListLoaderFragment.music.get(groupPosition).getAlbumName());
			startForeground(R.string.service_label, notification);
			if (android.os.Build.VERSION.SDK_INT >= 11) {
				GetNotificationThumbDrawable getNotificationThumbDrawable = new GetNotificationThumbDrawable(
						groupPosition, childPosition);
				getNotificationThumbDrawable.execute();
			}
		} catch (IllegalArgumentException | SecurityException | IllegalStateException | IOException e) {
			e.printStackTrace();
			Notification notification = getNotification("Error Playing: "
					+ MusicListLoaderFragment.music.get(groupPosition).getSongName(childPosition));
			startForeground(R.string.service_label, notification);
		}

	}

	@Override
	public void onCreate() {
		super.onCreate();
		L.d(DEBUG_TAG, "Service Started.");
		showNotification();
		isRunning = true;
	}

	private void showNotification() {
		String text = getText(R.string.service_started).toString();
		Notification notification = getNotification(text);
		startForeground(R.string.service_started, notification);
	}

	@SuppressWarnings("deprecation")
	private Notification getNotification(String text) {
		// In this sample, we'll use the same text for the ticker and the
		// expanded notification

		// Set the icon, scrolling text and timestamp
		Notification notification = new Notification(R.drawable.ic_launcher, text, System.currentTimeMillis());
		// The PendingIntent to launch our activity if the user selects this
		// notification
		PendingIntent contentIntent = PendingIntent.getActivity(
				this,
				0,
				new Intent(this, MusicActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

		// Set the info for the views that show in the notification panel.
		notification.setLatestEventInfo(this, getText(R.string.service_label), text, contentIntent);
		// Send the notification.
		// We use a layout id because it is a unique number. We use it later to
		// cancel.
		return notification;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		L.d(DEBUG_TAG, "Received start id " + startId + ": " + intent);

		if (intent.getAction().equals(ACTION_PLAY)) {
			mMediaPlayer = new MediaPlayer();
			mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		}

		return START_STICKY; // run until explicitly stopped.
	}

	public static boolean isRunning() {
		return isRunning;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// notification.
		stopForeground(true);
		if (mMediaPlayer != null) {
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
		try {
			unregisterReceiver(stopServiceReceiver);
			unregisterReceiver(pauseServiceReceiver);
			unregisterReceiver(playServiceReceiver);
		} catch (Exception e) {
		}
		L.d(DEBUG_TAG, "Service Stopped.");
		isRunning = false;
	}

	class GetNotificationThumbDrawable extends AsyncTask<String, Void, Bitmap> {
		int groupPosition;
		int childPosition;

		public GetNotificationThumbDrawable(int groupPosition, int childPosition) {
			this.groupPosition = groupPosition;
			this.childPosition = childPosition;
		}

		@SuppressLint("NewApi")
		@Override
		protected void onPostExecute(Bitmap result) {
			L.d(DEBUG_TAG, "onPostExecute");
			if (result != null) {
				PendingIntent contentIntent = PendingIntent.getActivity(MediaPlayerMainService.this, 0, new Intent(
						MediaPlayerMainService.this, MusicActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
				Notification notification = new Notification.BigPictureStyle(new Notification.Builder(
						getApplicationContext())
						.setContentTitle(getString(R.string.service_label))
						.setContentIntent(contentIntent)
						.setContentText(
								"Playing: "
										+ MusicListLoaderFragment.music.get(groupPosition).getSongName(childPosition)
										+ " - " + MusicListLoaderFragment.music.get(groupPosition).getAlbumName())
						.setSmallIcon(R.drawable.ic_launcher).setLargeIcon(result).addAction(0, "Play", playSelf())
						.addAction(0, "Pause", pauseSelf()).addAction(0, "Exit", killSelf())).bigPicture(result)
						.build();
				startForeground(R.string.service_label, notification);
			} else {
				L.d(DEBUG_TAG, "onPostExecute Result is Null");
			}
		}

		@Override
		protected Bitmap doInBackground(String... params) {
			try {
				String thumbCheck = MusicListLoaderFragment.music.get(groupPosition).getThumbnail();
				if (thumbCheck != null && thumbCheck.length() >= 1) {
					String url = MusicListLoaderFragment.music.get(groupPosition).getBaseUrl() + thumbCheck;
					L.d(DEBUG_TAG, "fetching image url " + url);
					return GetSongThumbDrawable.thumbFromUrl(getApplicationContext(), url);
				} else {
					return null;
				}
			} catch (IOException e) {
				L.d(DEBUG_TAG, "error thumbnail " + e.toString());
				e.printStackTrace();
				return null;
			}
		}

	}

	private PendingIntent killSelf() {
		L.d(DEBUG_TAG, "killSelf PendingIntent Sent");
		registerReceiver(stopServiceReceiver, new IntentFilter("myFilter"));
		PendingIntent contentIntent = PendingIntent.getBroadcast(this, 0, new Intent("myFilter"),
				PendingIntent.FLAG_UPDATE_CURRENT);
		return contentIntent;
	}

	private PendingIntent pauseSelf() {
		L.d(DEBUG_TAG, "pauseSelf PendingIntent Sent");
		registerReceiver(pauseServiceReceiver, new IntentFilter("myFilter2"));
		PendingIntent contentIntent = PendingIntent.getBroadcast(this, 0, new Intent("myFilter2"),
				PendingIntent.FLAG_UPDATE_CURRENT);
		return contentIntent;
	}

	private PendingIntent playSelf() {
		L.d(DEBUG_TAG, "playSelf PendingIntent Sent");
		registerReceiver(playServiceReceiver, new IntentFilter("myFilter3"));
		PendingIntent contentIntent = PendingIntent.getBroadcast(this, 0, new Intent("myFilter3"),
				PendingIntent.FLAG_UPDATE_CURRENT);
		return contentIntent;
	}

	// We need to declare the receiver with onReceive function as below
	protected BroadcastReceiver stopServiceReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			L.d(DEBUG_TAG, "stopServiceReceiver Recieved Killing Service");
			MediaPlayerMainService.this.stopForeground(true);
			MediaPlayerMainService.this.stopSelf();
		}
	};

	protected BroadcastReceiver pauseServiceReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context arg0, Intent arg1) {
			L.d(DEBUG_TAG, "pauseServiceReceiver Recieved Pausing Player");
			if (mMediaPlayer != null && mMediaPlayer.isPlaying())
				MediaPlayerMainService.this.mMediaPlayer.pause();
		}

	};

	protected BroadcastReceiver playServiceReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context arg0, Intent arg1) {
			L.d(DEBUG_TAG, "pauseServiceReceiver Recieved Pausing Player");
			if (mMediaPlayer != null && !mMediaPlayer.isPlaying())
				mMediaPlayer.start();
		}

	};
}
