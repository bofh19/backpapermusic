package com.w4rlock.backpapermusic.service;

import java.util.ArrayList;
import java.util.Iterator;

import com.w4rlock.backpapermusic.models.SongItem;

public class PlayList {
	public static final ArrayList<SongItem> playList = new ArrayList<SongItem>();
	private static final ArrayList<SongItem> playListPrevious = new ArrayList<SongItem>();

	public static SongItem getNextSongItem() {
		Iterator<SongItem> i = playList.iterator();
		if (i.hasNext()) {
			SongItem so = i.next();
			i.remove();
			addRemovedSongToPreviousList(so);
			return so;
		} else {
			return null;
		}
	}

	public static void moveNthPosToFirst(int n) {
		if (n < playList.size()) {
			playList.add(0, playList.remove(n));
		}
	}

	public static void addToPlayList(ArrayList<SongItem> songItems) {
		playList.addAll(songItems);
	}

	public static boolean bringPreviousSongToFrom() {
		Iterator<SongItem> i = playListPrevious.iterator();
		if (i.hasNext()) {
			SongItem so = i.next();
			i.remove();
			playList.add(0, so);
			return true;
		} else {
			return false;
		}
	}

	private static void addRemovedSongToPreviousList(SongItem songItem) {
		playListPrevious.add(songItem);
	}
}
