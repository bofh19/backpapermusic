package com.w4rlock.backpapermusic.service.binder;

import android.media.MediaPlayer;

import com.w4rlock.backpapermusic.models.SongItem;
import com.w4rlock.backpapermusic.service.google.MusicService.State;
import com.w4rlock.backpapermusic.util.L;

public class DummyMusicServiceCallback implements MusicServiceCallbackInterface {
	private static final String DEBUG_TAG = DummyMusicServiceCallback.class.toString();

	@Override
	public void onSongChanged(SongItem songItem) {
		L.d(DEBUG_TAG, "onSongChanged");
	}

	@Override
	public void onStateChanged(State state) {
		L.d(DEBUG_TAG, "onStateChanged: " + state.toString());
	}

	@Override
	public void onPause(SongItem songItem) {
		L.d(DEBUG_TAG, "onPause");
	}

	@Override
	public void onPlay(SongItem songItem, MediaPlayer mPlayer) {
		L.d(DEBUG_TAG, "onPlay");
	}

	@Override
	public void onMediaPlayerStop() {
		L.d(DEBUG_TAG, "onMediaPlayerStop");
	}

	@Override
	public void onSongCompletion() {
		L.d(DEBUG_TAG, "onSongCompletion");
	}

	@Override
	public void onPrepared(MediaPlayer mPlayer) {
		L.d(DEBUG_TAG, "onPrepared");
	}

	@Override
	public void onError(MediaPlayer mp, int what, int extra) {
		L.d(DEBUG_TAG, "onError");
	}

	@Override
	public void onMediaPlayerReleased() {
		L.d(DEBUG_TAG, "onMediaPlayerReleased");
	}

	@Override
	public void onMediaPlayerCreated() {
		L.d(DEBUG_TAG, "onMediaPlayerCreated");
	}

	@Override
	public void onPreparing(SongItem songItem) {
		L.d(DEBUG_TAG, "onPreparing");
	}

	@Override
	public void onBufferUpdate(MediaPlayer mp, int percent) {
		L.d(DEBUG_TAG, "onBufferUpdate " + percent);
	}

}
