package com.w4rlock.backpapermusic.service.binder;

import android.os.Binder;

import com.w4rlock.backpapermusic.service.google.MusicService;

public class MusicServiceBinder extends Binder {
	MusicService musicService;

	public MusicServiceBinder(MusicService musicService) {
		this.musicService = musicService;
	}

	public MusicService getService() {
		return musicService;
	}
}
