package com.w4rlock.backpapermusic.service.binder;

import android.media.MediaPlayer;

import com.w4rlock.backpapermusic.models.SongItem;
import com.w4rlock.backpapermusic.service.google.MusicService;

public interface MusicServiceCallbackInterface {
	public void onSongChanged(SongItem songItem);

	public void onStateChanged(MusicService.State state);

	public void onPause(SongItem songItem);

	public void onPlay(SongItem songItem, MediaPlayer mPlayer);

	public void onMediaPlayerStop();

	public void onSongCompletion();

	public void onPreparing(SongItem songItem);

	public void onPrepared(MediaPlayer mPlayer);

	public void onError(MediaPlayer mp, int what, int extra);

	public void onBufferUpdate(MediaPlayer mp, int percent);

	public void onMediaPlayerReleased();

	public void onMediaPlayerCreated();
}
