package com.w4rlock.backpapermusic.util;

import android.content.Context;
import android.graphics.Typeface;

public class ApplicationProperties {
	public static final String COMMON_FONT_STYLE = "fonts/RobotoCondensed-Regular.ttf";
	public static final String COMMON_FONT_STYLE_BOLD = "fonts/RobotoCondensed-Bold.ttf";

	public static Typeface getTypeface(Context ctx) {
		return Typeface.createFromAsset(ctx.getAssets(), COMMON_FONT_STYLE);
	}
}
