package com.w4rlock.backpapermusic.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.TypedValue;

public class GetSongThumbDrawable {
	public static Bitmap thumbFromUrl(String url) throws java.net.MalformedURLException, java.io.IOException {
		Bitmap x;
		HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
		connection.setRequestProperty("User-agent", "Mozilla/4.0");
		connection.connect();
		InputStream input = connection.getInputStream();
		x = BitmapFactory.decodeStream(input);
		return x;
	}

	public static Bitmap thumbFromUrl(Context context, String imageUrl) throws java.net.MalformedURLException,
			java.io.IOException {
		File imageFile;
		try {
			imageFile = getFileFromUrl(imageUrl, context);
		} catch (Exception e) {
			L.d("thumbFromUrl", "looks like file does not exist in cache. trying to get from url");

			URL url = new URL(imageUrl);
			InputStream is = url.openStream();
			try {
				addtoCacheFile(is, imageUrl, context);
				imageFile = getFileFromUrl(imageUrl, context);
			} catch (Exception e1) {
				L.d("thumbFromUrl", "Unable to add to cache going hardway " + e1.toString());

				File cacheDir = new File(context.getCacheDir().getAbsoluteFile() + "/thumbs");
				if (!cacheDir.exists())
					cacheDir.mkdirs();
				imageFile = new File(cacheDir + "/" + imageUrl.hashCode());
				imageFile.deleteOnExit();
				L.d("thumbFromUrl", "imageFile " + imageFile.getAbsolutePath() + " Status " + imageFile.exists());
				is = url.openStream();
				OutputStream os = new FileOutputStream(imageFile);
				byte[] b = new byte[2048];
				int length;

				while ((length = is.read(b)) != -1) {
					os.write(b, 0, length);
				}
				is.close();
				os.close();
			}
			L.d("thumbFromUrl", "Saved Image at " + imageFile.getAbsolutePath());
		}

		L.d("thumbFromUrl", "Reading Image at " + imageFile.getAbsolutePath());

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		return BitmapFactory.decodeFile(imageFile.getAbsolutePath(), options);
	}

	public static Boolean addtoCacheFile(InputStream is, String url, Context context) throws Exception {
		File cacheDir;

		cacheDir = StorageUtils.getCacheDirectory(context);

		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
			cacheDir = new File(android.os.Environment.getExternalStorageDirectory(),
					"com.w4rlock.backpapermusic.location");
		else
			cacheDir = context.getCacheDir();
		if (!cacheDir.exists())
			cacheDir.mkdirs();

		String filename = String.valueOf(url.hashCode());
		File f = new File(cacheDir, filename);
		OutputStream os = new FileOutputStream(f);
		byte[] b = new byte[2048];
		int length;

		while ((length = is.read(b)) != -1) {
			os.write(b, 0, length);
		}
		is.close();
		os.close();
		return true;
	}

	public static String readFromFile(String path) throws IOException {
		FileInputStream stream = new FileInputStream(new File(path));
		try {
			FileChannel fc = stream.getChannel();
			MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
			/* Instead of using default, pass in a decoder. */
			return Charset.defaultCharset().decode(bb).toString();
		} finally {
			stream.close();
		}
	}

	public static File getFileFromUrl(String url, Context context) throws IOException {

		File cacheDir;

		cacheDir = StorageUtils.getCacheDirectory(context);

		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
			cacheDir = new File(android.os.Environment.getExternalStorageDirectory(),
					"com.w4rlock.backpapermusic.location");
		else
			cacheDir = context.getCacheDir();
		if (!cacheDir.exists())
			cacheDir.mkdirs();

		String filename = String.valueOf(url.hashCode());
		File f = new File(cacheDir, filename);
		if (f.exists()) {
			FileReader fr = new FileReader(f);
			if (fr.read() == -1) {
				fr.close();
				throw new IOException("Unable to read file");
			} else {
				try {
					readFromFile(f.getAbsolutePath());
					return f;
				} catch (IOException e) {
					L.d("thumbFromUrl", "error while reading cache file after its not empty " + e.toString());
					throw e;
				} finally {
					fr.close();
				}
			}
		} else {
			throw new IOException("DOES NOT EXISTS");
		}
	}

	public static String readFromUrl(String url, Context context) throws IOException {
		String res1 = "";

		File cacheDir;

		cacheDir = StorageUtils.getCacheDirectory(context);

		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
			cacheDir = new File(android.os.Environment.getExternalStorageDirectory(),
					"com.w4rlock.backpapermusic.location");
		else
			cacheDir = context.getCacheDir();
		if (!cacheDir.exists())
			cacheDir.mkdirs();

		String filename = String.valueOf(url.hashCode());
		File f = new File(cacheDir, filename);
		if (f.exists()) {
			FileReader fr = new FileReader(f);
			if (fr.read() == -1) {
				fr.close();
				throw new IOException("Unable to read file");
			} else {
				try {
					res1 = readFromFile(f.getAbsolutePath());
				} catch (IOException e) {
					L.d("thumbFromUrl", "error while reading cache file after its not empty " + e.toString());
					throw e;
				} finally {
					fr.close();
				}
			}
		} else {
			throw new IOException("DOES NOT EXISTS");
		}
		return res1;
	}

	public static float getPxFromDp(int dp, Context context) {
		Resources r = context.getResources();
		return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
	}

	// IMAGE LOGICS

	public static enum ScalingLogic {
		CROP, FIT
	}

	public static Bitmap decodeResourceFromImage(String path, int dstWidth, int dstHeight, ScalingLogic scalingLogic) {
		Options options = new Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, options);
		options.inJustDecodeBounds = false;
		options.inSampleSize = calculateSampleSize(options.outWidth, options.outHeight, dstWidth, dstHeight,
				scalingLogic);
		Bitmap unscaledBitmap = BitmapFactory.decodeFile(path, options);
		return unscaledBitmap;
	}

	public static int calculateSampleSize(int srcWidth, int srcHeight, int dstWidth, int dstHeight,
			ScalingLogic scalingLogic) {
		if (scalingLogic == ScalingLogic.FIT) {
			final float srcAspect = (float) srcWidth / (float) srcHeight;
			final float dstAspect = (float) dstWidth / (float) dstHeight;

			if (srcAspect > dstAspect) {
				return srcWidth / dstWidth;
			} else {
				return srcHeight / dstHeight;
			}
		} else {
			final float srcAspect = (float) srcWidth / (float) srcHeight;
			final float dstAspect = (float) dstWidth / (float) dstHeight;

			if (srcAspect > dstAspect) {
				return srcHeight / dstHeight;
			} else {
				return srcWidth / dstWidth;
			}
		}
	}

	public static Bitmap createScaledBitmap(Bitmap unscaledBitmap, int dstWidth, int dstHeight,
			ScalingLogic scalingLogic) {
		Rect srcRect = calculateSrcRect(unscaledBitmap.getWidth(), unscaledBitmap.getHeight(), dstWidth, dstHeight,
				scalingLogic);
		Rect dstRect = calculateDstRect(unscaledBitmap.getWidth(), unscaledBitmap.getHeight(), dstWidth, dstHeight,
				scalingLogic);
		Bitmap scaledBitmap = Bitmap.createBitmap(dstRect.width(), dstRect.height(), Config.ARGB_8888);
		Canvas canvas = new Canvas(scaledBitmap);
		canvas.drawBitmap(unscaledBitmap, srcRect, dstRect, new Paint(Paint.FILTER_BITMAP_FLAG));

		return scaledBitmap;
	}

	public static Rect calculateSrcRect(int srcWidth, int srcHeight, int dstWidth, int dstHeight,
			ScalingLogic scalingLogic) {
		if (scalingLogic == ScalingLogic.CROP) {
			final float srcAspect = (float) srcWidth / (float) srcHeight;
			final float dstAspect = (float) dstWidth / (float) dstHeight;

			if (srcAspect > dstAspect) {
				final int srcRectWidth = (int) (srcHeight * dstAspect);
				final int srcRectLeft = (srcWidth - srcRectWidth) / 2;
				return new Rect(srcRectLeft, 0, srcRectLeft + srcRectWidth, srcHeight);
			} else {
				final int srcRectHeight = (int) (srcWidth / dstAspect);
				final int scrRectTop = (int) (srcHeight - srcRectHeight) / 2;
				return new Rect(0, scrRectTop, srcWidth, scrRectTop + srcRectHeight);
			}
		} else {
			return new Rect(0, 0, srcWidth, srcHeight);
		}
	}

	public static Rect calculateDstRect(int srcWidth, int srcHeight, int dstWidth, int dstHeight,
			ScalingLogic scalingLogic) {
		if (scalingLogic == ScalingLogic.FIT) {
			final float srcAspect = (float) srcWidth / (float) srcHeight;
			final float dstAspect = (float) dstWidth / (float) dstHeight;

			if (srcAspect > dstAspect) {
				return new Rect(0, 0, dstWidth, (int) (dstWidth / srcAspect));
			} else {
				return new Rect(0, 0, (int) (dstHeight * srcAspect), dstHeight);
			}
		} else {
			return new Rect(0, 0, dstWidth, dstHeight);
		}
	}
}
