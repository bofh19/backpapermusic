package com.w4rlock.backpapermusic.util;

import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.os.Build;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.w4rlock.backpapermusic.R;
import com.w4rlock.backpapermusic.service.google.MusicService;

public class Utils {
	public static final int CurrentApiVersion = Build.VERSION.SDK_INT;
	public static final int HoneyCombApiVersion = Build.VERSION_CODES.HONEYCOMB;

	public static boolean isOverHoneyComb() {
		return CurrentApiVersion >= HoneyCombApiVersion;
	}

	public static boolean canShowBigPictureStyleNotification() {
		return CurrentApiVersion >= 16;
	}

	public static boolean canHaveRemoteControl() {
		return CurrentApiVersion >= 14;
	}

	public static boolean isServiceRunning(Context context, String serviceName) {
		boolean serviceRunning = false;
		ActivityManager am = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
		List<ActivityManager.RunningServiceInfo> l = am.getRunningServices(50);
		Iterator<ActivityManager.RunningServiceInfo> i = l.iterator();
		while (i.hasNext()) {
			ActivityManager.RunningServiceInfo runningServiceInfo = (ActivityManager.RunningServiceInfo) i.next();

			if (runningServiceInfo.service.getClassName().equals(serviceName)) {
				serviceRunning = true;
			}
		}
		return serviceRunning;
	}

	public static boolean isMusicServiceRunning(Context context) {
		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (MusicService.class.getName().equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	public static DisplayImageOptions getDisplayImageOptions() {
		return new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.ic_launcher)
				.showImageForEmptyUri(R.drawable.ic_launcher).showImageOnFail(R.drawable.ic_launcher)
				.cacheInMemory(true).cacheOnDisc(true).considerExifParams(true).build();
	}
}
