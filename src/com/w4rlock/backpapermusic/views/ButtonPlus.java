package com.w4rlock.backpapermusic.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

import com.w4rlock.backpapermusic.util.ApplicationProperties;

public class ButtonPlus extends Button {

	public ButtonPlus(Context context) {
		super(context);
		if (!isInEditMode())
			this.setTypeface(ApplicationProperties.getTypeface(context));
	}

	public ButtonPlus(Context context, AttributeSet attrs) {
		super(context, attrs);
		if (!isInEditMode())
			this.setTypeface(ApplicationProperties.getTypeface(context));
	}

	public ButtonPlus(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		if (!isInEditMode())
			this.setTypeface(ApplicationProperties.getTypeface(context));
	}
}