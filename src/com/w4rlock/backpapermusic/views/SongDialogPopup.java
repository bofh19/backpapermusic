package com.w4rlock.backpapermusic.views;

import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.w4rlock.backpapermusic.R;
import com.w4rlock.backpapermusic.adapters.SongItemListDiaplayAdapter;
import com.w4rlock.backpapermusic.models.SongItem;
import com.w4rlock.backpapermusic.util.L;
import com.w4rlock.backpapermusic.util.Utils;

public class SongDialogPopup extends Dialog {
	private ArrayList<SongItem> songs;
	private OnSongItemClicked mOnSongItemClicked;
	ImageView mHeadingIcon;
	TextView mHeadingText;
	ListView mListView;
	Button mAddButton;
	SongItemListDiaplayAdapter mSongItemListDiaplayAdapter;
	DisplayImageOptions doptions = Utils.getDisplayImageOptions();
	ImageLoader imageLoader;
	private String DEBUG_TAG = "SongDialogPopup";

	public SongDialogPopup(Context context, ArrayList<SongItem> songs, OnSongItemClicked onSongItemClicked) {
		super(context);
		this.songs = songs;
		this.mOnSongItemClicked = onSongItemClicked;
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.song_dialog_popup);
		if (songs.size() > 0)
			DEBUG_TAG += " [" + songs.get(0).getAlbum() + "]";
		mSongItemListDiaplayAdapter = new SongItemListDiaplayAdapter(context, songs);

		mListView = (ListView) findViewById(R.id.alert_dialog_list);
		mHeadingIcon = (ImageView) findViewById(R.id.alert_dialog_heading_icon);
		mHeadingText = (TextView) findViewById(R.id.alert_dialog_heading);
		mAddButton = (Button) findViewById(R.id.alert_dialog_add);

		imageLoader = ImageLoader.getInstance();
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).build();
		imageLoader.init(config);

		((Button) findViewById(R.id.alert_dialog_cancel)).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
	}

	@Override
	public void show() {
		L.d(DEBUG_TAG, "showing pop up");
		mListView.setAdapter(mSongItemListDiaplayAdapter);
		if (this.songs.size() > 0) {
			mHeadingText.setText(songs.get(0).getAlbum());
			imageLoader.displayImage(songs.get(0).getThumbUrl(), mHeadingIcon, doptions);
		}
		mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				SongItem songItem = (SongItem) parent.getItemAtPosition(position);
				songItem.isSelected = !songItem.isSelected;
				mSongItemListDiaplayAdapter.notifyDataSetChanged();
			}
		});

		mAddButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				ArrayList<SongItem> songitems = new ArrayList<SongItem>();
				for (int i = 0; i < mSongItemListDiaplayAdapter.getCount(); i++) {
					SongItem si = mSongItemListDiaplayAdapter.getItem(i);
					if (si.isSelected)
						songitems.add(si);
				}
				L.d(DEBUG_TAG, "selected " + songitems.size());
				mOnSongItemClicked.onSongItemClicked(songitems);
				dismiss();
			}
		});

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.MATCH_PARENT;
		super.show();
		super.getWindow().setAttributes(lp);
	}

	@Override
	public void dismiss() {
		mListView.setAdapter(null);
		mListView.setOnItemClickListener(null);
		mSongItemListDiaplayAdapter = null;
		super.dismiss();
	}

	public static interface OnSongItemClicked {
		public void onSongItemClicked(ArrayList<SongItem> songItems);
	}

}
