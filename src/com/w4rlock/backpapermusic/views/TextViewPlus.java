package com.w4rlock.backpapermusic.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.w4rlock.backpapermusic.util.ApplicationProperties;

public class TextViewPlus extends TextView {

	public TextViewPlus(Context context, AttributeSet attrs) {
		super(context, attrs);
		if (!isInEditMode())
			this.setTypeface(ApplicationProperties.getTypeface(context));
	}

	public TextViewPlus(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		if (!isInEditMode())
			this.setTypeface(ApplicationProperties.getTypeface(context));
	}

	public TextViewPlus(Context context) {
		super(context);
		if (!isInEditMode())
			this.setTypeface(ApplicationProperties.getTypeface(context));
	}

}